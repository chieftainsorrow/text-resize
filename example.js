let el = document.getElementById("e");
let s = 25;
el.style.fontSize = s + "px";
let reduce_size = function(){
  if (el.scrollHeight - el.clientHeight > 0) {
    s -= 0.5;
    el.style.fontSize = s + "px";
    setTimeout(reduce_size, 100);
  }
};

setTimeout(reduce_size, 500);
